var express2 = require("express");
var Admin = require('../models/admin');
var adminService = require('../services/adminService');
var bodyParser = require('body-parser');
var config = require('../config');
var path = require('path');

var fileUpload = require('express-fileupload');


var multer = require('multer')

var storage1 = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/profilepic/')
    },
    filename: function (req, file, cb) {
        let split = file.mimetype.split("/");
        let timeStamp = Date.now();
        console.log('files => ', file);

        cb(null, `${file.fieldname}_${timeStamp}.${split[1]}`)
    }
})


module.exports = function () {
    // admin.use(upload());
    var admin = express2.Router();
    // admin.use(bodyParser.json());
    // admin.use(bodyParser.urlencoded({
    //     extended: true
    // }));
    //admin.use(fileUpload());


    admin.post('/adminSignup', function (req, res) {
        var adminData = req.body;
        //  console.log(adminData);
        adminService.adminSignup(adminData, function (response) {
            res.send(response);
        });
    });
    admin.post('/adminLogin', function (req, res) {
        adminService.adminLogin(req.body, function (loginRes) {
            res.send(loginRes);
        })
    });

    admin.post('/create-content', function (req, res) {
        // console.log('body', req.body)
        adminService.createContent(req.body, function (content) {
            res.send(content)
        })
    })
    admin.post('/fetch-content', function (req, res) {
        adminService.fetchContent(req.body, function (content) {
            res.send(content)
        })
    })
    admin.post('/update-content', function (req, res) {
        adminService.updateContent(req.body, function (content) {
            res.send(content)
        })
    })
    admin.post('/contents', function (req, res) {
        adminService.contents(function (content) {
            res.send(content)
        })
    })
    admin.post('/delete-content', function (req, res) {
        adminService.deleteContent(req.body, function (content) {
            res.send(content)
        })
    })

    admin.post('/providers', function (req, res) {
        let data = req.body;
        adminService.providers(data, function (result) {
            res.send(result)
        })
    })


    return admin;
}