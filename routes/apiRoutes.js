'use strict';
// var fs = require('fs');
var express2 = require("express");
var apiService = require('../services/apiService');
var mongo = require('mongodb');
var ObjectID = mongo.ObjectID;
var multer = require('multer')
var bcrypt = require('bcrypt-nodejs');

var UserModels = require('../models/user');


var authenticate = require('../services/authService');
const {
    callbackPromise
} = require("nodemailer/lib/shared");
const {
    json
} = require("body-parser");

var patient = multer.diskStorage({
    destination: function (req, file, cb) {
        if (file.fieldname == 'image') {
            cb(null, './public/uploads/profile_images/')
        } else {
            cb(null, './public/uploads/patient/')
        }

    },
    filename: function (req, file, cb) {
        let split = file.mimetype.split("/");
        let timeStamp = Date.now();
        // console.log('files => ', file);

        cb(null, `${file.fieldname}_${timeStamp}.${split[1]}`)
    }
})

var laboratory = multer.diskStorage({
    destination: function (req, file, cb) {
        if (file.fieldname == 'image') {
            cb(null, './public/uploads/profile_images/')
        } else {
            cb(null, './public/uploads/laboratories/')
        }

    },
    filename: function (req, file, cb) {
        let split = file.mimetype.split("/");
        let timeStamp = Date.now();
        // console.log('files => ', file);

        cb(null, `${file.fieldname}_${timeStamp}.${split[1]}`)
    }
})
var doctor = multer.diskStorage({

    destination: function (req, file, cb) {
        if (file.fieldname == 'certificates') {
            cb(null, './public/uploads/doctor/')
        }
        if (file.fieldname == 'image') {
            cb(null, './public/uploads/profile_images/')
        } else {
            cb(null, './public/uploads/doctor/')
        }
    },
    filename: function (req, file, cb) {
        let split = file.mimetype.split("/");
        let timeStamp = Date.now();
        cb(null, `${file.fieldname}_${timeStamp}.${split[1]}`)
    }
})

var profile = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/profile_images/')
    },
    filename: function (req, file, cb) {
        let split = file.mimetype.split("/");
        let timeStamp = Date.now();
        // console.log('files => ', file);

        cb(null, `${file.fieldname}_${timeStamp}.${split[1]}`)
    }
})

module.exports = function (app, express) {

    var api = express2.Router();

    api.post('/register-patient', function (req, res) {
        let data = req.body;
        apiService.signupUser(data, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })
    api.post('/register-doctor', multer({
        storage: doctor
    }).fields([{
        name: 'certificates',
        maxCount: 10
    }, {
        name: 'image',
        maxCount: 1
    }]), function (req, res) {
        let data = req.body;
        let doctor = {};
        let doctor_experiences = [];


        doctor.user_type = data.user_type;
        doctor.age = data.age;
        doctor.phone = data.phone;
        doctor.email = data.email;
        doctor.address = data.address;
        doctor.full_name = data.full_name;
        doctor.gender = data.gender;
        doctor.certificates = data.certificates;
        doctor.diploma = data.diploma;
        doctor.language = data.language;
        doctor.works_publication = data.works_publication;
        doctor.country = data.country;
        doctor.bio = data.bio;
        doctor.location_lat = (data.location_lat != 'undefined') ? data.location_lat : null;
        doctor.location_long = (data.location_long != 'undefined') ? data.location_long : null;

        data.experiences.forEach((e) => {
            let exp = JSON.parse(e)
            doctor_experiences.push({
                hospital_name: exp.hospital_name,
                start_date: exp.start_date,
                end_date: exp.end_date,
                currently_employed: exp.currently_employed,
                _id: new ObjectID()
            })
        })
        doctor.password = data.password;
        doctor.experiences = doctor_experiences;

        if (req.files) {
            var image_url = ''
            // experiences_obj = data.experiences
            let uploadedFile = req.files;
            if (req.files.image) {
                doctor.image_url = `/uploads/profile_images/${req.files.image[0].filename}`


            }
            if (req.files.certificates) {
                var result = Object.keys(uploadedFile).map(function (key) {
                    return uploadedFile[key];
                });
                let documents = [];
                result.forEach((file) => {
                    let certificate_url = `/uploads/doctor/${file.filename}`;
                    console.log('file', file);

                    documents.push({
                        certificate_url: certificate_url
                    })
                })

                doctor.certificates = documents
            }


            //data.experiences = experiences_obj
            apiService.registerDoctor(doctor, function (result) {

                res.status(result.response_code).send(result.response_data);
            })
        } else {
            apiService.registerDoctor(doctor, function (result) {

                res.status(result.response_code).send(result.response_data);
            })
        }
        //});


    })
    api.post('/register-laboratory', multer({
        storage: laboratory
    }).fields([{
        name: 'image',
        maxCount: 1
    }]), function (req, res) {
        let data = req.body;
        let user = {};
        //bcrypt.hash(req.password, null, null, function (err, hash) {

        if (req.files) {
            if (req.files.image) {
                user.image_url = `/uploads/profile_images/${req.files.image[0].filename}`


            }
        }
        let testing_facilities = []
        user.user_type = data.user_type;

        user.phone = data.phone;
        user.email = data.email;
        user.address = data.address;
        user.full_name = data.full_name;
        user.country = data.country;
        data.testing_facilities.forEach((e) => {
            let exp = JSON.parse(e)
            testing_facilities.push({
                name: exp.name,
                price: exp.price,
                _id: new ObjectID()
            })
        })
        user.password = data.password;
        user.testing_facilities = testing_facilities;
        user.location_lat = (user.location_lat != 'undefined') ? user.location_lat : null;
        user.location_long = (user.location_long != 'undefined') ? user.location_long : null;
        apiService.registerLaboratory(user, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
        //})
    })
    api.post('/login', function (req, res) {
        // console.log('login', req);
        apiService.login(req.body, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    });
    api.post('/chief-complaint', authenticate, multer({
        storage: patient
    }).fields([{
        name: 'documents',
        maxCount: 10
    }]), function (req, res) {
        let data = req.body;
        let patient_splt = []
        let rglgy = []
        data.radiology.forEach((e) => {
            let exp = JSON.parse(e)
            rglgy.push({
                name: exp.name,
                _id: new ObjectID()
            })
        })
        data.patient_speciality.forEach((e) => {
            let exp = JSON.parse(e)
            patient_splt.push({
                name: exp.name,
                _id: new ObjectID()
            })
        })
        var patient = {
            id: data.id,
            auth_token: data.auth_token,
            user_type: data.user_type,
            patient_speciality: patient_splt,
            medical_condition: data.medical_condition,
            laboratory: data.laboratory,
            radiology: rglgy,
            allergy: data.allergy,
            problem_description: data.problem_description,
            problem_summary: data.problem_summary,
            age: data.age,
            phone: data.phone,
            email: data.email,
            address: data.address,
            full_name: data.full_name,
            gender: data.gender,
            pun: Math.floor(Math.random() * 1000000000)
        }
        if (req.files) {
            let uploadedFile = req.files;
            var result = Object.keys(uploadedFile).map(function (key) {
                return uploadedFile[key];
            });
            let documents = [];
            result.forEach((file) => {
                let doc_url = `/uploads/patient/${file.filename}`;
                console.log('file', file);

                documents.push({
                    doc_url: doc_url
                })
            })
            patient.patients_documets = documents
            apiService.completePatientProfile(patient, function (result) {

                res.status(result.response_code).send(result.response_data);
            })
        } else {
            apiService.completePatientProfile(patient, function (result) {

                res.status(result.response_code).send(result.response_data);
            })
        }

    })
    api.post('/upload-user-image', multer({
        storage: profile
    }).single('image'), function (req, res) {

        var data = req.body;
        //console.log(req.files, req.body.id);
        if (!data.id || typeof data.id === undefined) {
            res.send({
                status: false,
                message: 'Please provide user Id',
                data: {}
            })
        } else {
            if (req.file) {
                let image_url = `/uploads/profile_images/${req.file.filename}`;

                // res.send({user:userData});
                //data.image_url = image_url;
                UserModels.findOne({
                    _id: data.id
                }, (err, user) => {
                    if (user) {
                        user.image_url = image_url
                        user.save((err, saved) => {
                            if (err) {
                                res.send({
                                    status: false,
                                    message: "Internal server error",
                                    data: {}
                                })
                            } else {
                                res.send({
                                    status: true,
                                    message: "Profile image saved",
                                    data: {}
                                })
                            }
                        })
                    } else if (err) {
                        res.send({
                            status: false,
                            message: "Internal server error",
                            data: {}
                        })
                    } else {
                        res.send({
                            status: false,
                            message: "User not found",
                            data: {}
                        })
                    }
                })

            }
        }

    })
    api.post('/update-profile-patient', authenticate, multer({
        storage: patient
    }).fields([{
            name: 'documents',
            maxCount: 10
        },
        {
            name: 'image',
            maxCount: 1
        }
    ]), function (req, res) {
        let data = req.body;
        var patient = {
            id: data.id,
            medical_condition: data.medical_condition,
            age: data.age,
            phone: data.phone,
            email: data.email,
            address: data.address,
            full_name: data.full_name,
            gender: data.gender,
            health_insurance: data.health_insurance,
            dob: data.dob
        }
        if (req.files) {
            console.log('files', req.files);
            if (req.files.image) {
                patient.image_url = `/uploads/profile_images/${req.files.image[0].filename}`


            }

            if (req.files.documents) {
                let uploadedFile = req.files.documents;
                var result = Object.keys(uploadedFile).map(function (key) {
                    return uploadedFile[key];
                });
                let documents = [];
                result.forEach((file) => {
                    let doc_url = `/uploads/patient/${file.filename}`;
                    console.log('file', file);

                    documents.push({
                        doc_url: doc_url
                    })
                })
                patient.patients_documets = documents
            }
            apiService.completePatientProfile(patient, function (result) {

                res.status(result.response_code).send(result.response_data);
            })
        } else {
            apiService.completePatientProfile(patient, function (result) {

                res.status(result.response_code).send(result.response_data);
            })
        }

    })
    api.post('/update-profile-doctor', authenticate, multer({
        storage: doctor
    }).fields([{
            name: 'resume',
            maxCount: 1
        },
        {
            name: 'image',
            maxCount: 1
        }
    ]), function (req, res) {
        let data = req.body;
        let doctor = {};


        doctor.id = data.id;
        doctor.phone = data.phone;
        doctor.email = data.email;
        doctor.address = data.address;
        doctor.bio = data.bio;
        doctor.weekly_avalaibility = JSON.parse(data.weekly_avalaibility)
        doctor.education = data.education;
        doctor.bank_details = JSON.parse(data.bank_details);
        doctor.bio = data.bio;



        if (req.files) {
            if (req.files.image) {
                doctor.image_url = `/uploads/profile_images/${req.files.image[0].filename}`


            }
            if (req.files.resume) {
                doctor.resume_url = `/uploads/doctor/${req.files.resume[0].filename}`


            }

            apiService.updateProfileDoctor(doctor, function (result) {

                res.status(result.response_code).send(result.response_data);
            })
        } else {
            apiService.updateProfileDoctor(re.body, function (result) {
                res.status(result.response_code).send(result.response_data);
            })
        }

    })
    api.post('/update-profile-laboratory', multer({
        storage: doctor
    }).fields([{
        name: 'image',
        maxCount: 1
    }]), authenticate, function (req, res) {
        let data = req.body;
        var testing_facilities = []
        data.testing_facilities.forEach((e) => {
            //let exp = JSON.parse(e)
            testing_facilities.push({
                name: e.name,
                price: e.price,
                _id: new ObjectID()
            })
        })


        // let {
        //     sunday,
        //     monday,
        //     tuesday,
        //     wednesday,
        //     thursday,
        //     friday,
        //     saturday
        // } = data.weekly_avalaibility

        // let weekly_avalaibility = {
        //     sunday: {
        //         start_time: sunday.start_time,
        //         start_time: sunday.start_time,
        //         available: sunday.available
        //     },
        //     monday: {
        //         start_time: monday.start_time,
        //         start_time: monday.start_time,
        //         available: monday.available
        //     },
        //     tuesday: {
        //         start_time: tuesday.start_time,
        //         start_time: tuesday.start_time,
        //         available: tuesday.available
        //     },
        //     wednesday: {
        //         start_time: wednesday.start_time,
        //         start_time: wednesday.start_time,
        //         available: wednesday.available
        //     },
        //     thursday: {
        //         start_time: thursday.start_time,
        //         start_time: thursday.start_time,
        //         available: thursday.available
        //     },
        //     friday: {
        //         start_time: friday.start_time,
        //         start_time: friday.start_time,
        //         available: friday.available
        //     },
        //     saturday: {
        //         start_time: saturday.start_time,
        //         start_time: saturday.start_time,
        //         available: saturday.available
        //     }
        // }

        let user = {
            id: data.id,
            phone: data.phone,
            email: data.email,
            address: data.address,
            weekly_avalaibility: JSON.parse(data.weekly_avalaibility),
            bank_details: data.bank_details,
            testing_facilities: testing_facilities
        };
        if (req.files) {
            if (req.files.image) {
                user.image_url = `/uploads/profile_images/${req.files.image[0].filename}`
            }
        }

        apiService.updateProfileLaboratory(user, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })
    api.post('/scheduleAppointment', authenticate, function (req, res) {
        let data = req.body;
        console.log('scheduleAppointment', data)
        apiService.scheduleAppointment(data, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })
    api.post('/bookingDetails', authenticate, function (req, res) {
        let data = req.body;
        apiService.bookingDetails(data, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })
    api.post('/listBokkings', authenticate, function (req, res) {
        let data = req.body;
        apiService.listBokkings(data, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })
    api.post('/providers', authenticate, function (req, res) {
        let data = req.body;
        apiService.providers(data, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })

    api.post('/logout', function (req, res) {
        apiService.logout(req.body, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    });

    api.post('/change-password', function (req, res) {
        apiService.changePassword(req.body, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })

    api.post('/forgot-password', function (req, res) {
        apiService.forgotPassword(req.body, function (passwordRes) {
            res.status(result.response_code).send(result.response_data);
        })
    });
    api.post('/refresh-token', (req, res) => {
        const postData = req.body
        apiService.refreshToken(postData, function (result) {
            res.status(result.response_code).send(result.response_data)
        })

    })

    api.post('/verify-code', function (req, res) {
        apiService.verifyCode(req.body, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })

    api.post('/search', function (req, res) {
        apiService.search(req.body, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })

    api.get('/contents', function (req, res) {
        apiService.contents(function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })

    api.post('/patient-details', authenticate, function (req, res) {
        apiService.patientDetails(req.body, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })

    api.post('/provider-details', authenticate, function (req, res) {
        apiService.providerDetails(req.body, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })

    api.post('/weekly-calender', function (req, res) {
        apiService.weeklyCalender(req.body, function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })

    api.post('/testToken', authenticate, function (req, res) {
        res.send({
            status: true
        })
    })
    api.get('/countries', function (req, res) {
        apiService.countries(function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })
    api.get('/test-facilities', function (req, res) {
        apiService.testFacilities(function (result) {
            res.status(result.response_code).send(result.response_data);
        })
    })
    api.post('/add-countries', function (req, res) {
        apiService.addCountries(function (result) {
            res.send(result)
        })
    })
    api.get('/patient-speciality', function (req, res) {

        res.status(200).send({
            response_code: 200,
            response_data: {
                status: true,
                message: "Patient Speciality",
                data: [{
                        name: "ABC"
                    },
                    {
                        name: "BCD"
                    }
                ]
            }
        })
    })
    api.get('/radiologies', function (req, res) {

        res.status(200).send({
            response_code: 200,
            response_data: {
                status: true,
                message: "Radiologies List",
                data: [{
                        name: "CT"
                    },
                    {
                        name: "MRI"
                    }
                ]
            }
        })
    })
    return api;
}