'use strict';
// var express = require("express");
// var config = require('../config');
var async = require("async");
var mongo = require('mongodb');
var crypto = require('crypto');
var sha1 = require('node-sha1');
// var fs = require('fs');
// var util = require('util');
// var logger = require('morgan');
//var mailProperty = require('../modules/sendMail');
// var pushNotification = require('../modules/pushNotification');
var ObjectID = mongo.ObjectID;
//var baseUrl = config.baseUrl;

var jwt = require('jsonwebtoken');
var {
    authentication
} = require('../config');

var fileUpload = require('express-fileupload');


var tokenList = []
//express().use(fileUpload());
//======================MONGO MODELS============================

var UserModels = require('../models/user');
var BookingModels = require('../models/booking');
var ContentModels = require('../models/content');
const CountryModels = require("../models/country");

//======================MONGO MODELS============================

var apiService = {
    jwtAuthVerification: (jwtData, callback) => {
        UserModels.authenticate(jwtData, function (auth) {
            callback(auth);
        })
    },
    login: (loginData, callback) => {
        if (!loginData.email || typeof loginData.email === undefined) {
            callback({
                status: "error",
                message: "please provide email",
                data: {}
            })
        } else if (!loginData.password || typeof loginData.password === undefined) {
            callback({
                status: "error",
                message: "please provide password",
                data: {}
            })
        } else {
            var token = jwt.sign({
                email: loginData.email
            }, authentication.secret, {
                expiresIn: authentication.tokenLife
            });

            var refreshToken = jwt.sign({
                email: loginData.email
            }, authentication.refreshTokenSecret, {
                expiresIn: authentication.refreshTokenLife
            });
            const response = {
                "status": "Logged in",
                "token": token,
                "refreshToken": refreshToken,
            }
            tokenList[refreshToken] = response
            console.log(token);
            loginData.auth_token = token;
            UserModels.login(loginData, function (loginInfo) {
                callback(loginInfo)

            })
        }

    },

    refreshToken: (data, callback) => {
        if ((data.refreshToken) && (data.refreshToken in tokenList)) {

            var token = jwt.sign({
                email: data.email
            }, authentication.secret, {
                expiresIn: authentication.tokenLife
            });

            // update the token in the list
            tokenList[data.refreshToken].token = token
            callback({
                response_code: 200,
                response_data: {
                    status: true,
                    auth_token: token
                }
            })
        } else {
            callback({
                response_code: 404,
                response_data: {
                    status: false,
                    message: "refresh token not found",
                    data: {}
                }
            })
        }
    },
    signupUser: (userData, callback) => {
        if (!userData.full_name || typeof userData.full_name === undefined) {
            callback({
                status: "error",
                message: "please provide user name",
                data: {}
            })
        } else if (!userData.email || typeof userData.email === undefined) {
            callback({
                status: "error",
                message: "please provide email",
                data: {}
            })
        } else if (!userData.password || typeof userData.password === undefined) {
            callback({
                status: "error",
                message: "please provide password",
                data: {}
            })
        } else {
            userData._id = new ObjectID;
            var token = jwt.sign({
                email: userData.email
            }, authentication.secret, {
                expiresIn: authentication.tokenLife
            });
            console.log(token);
            userData.auth_token = token;
            var sh1Pass = Math.floor(100000 + Math.random() * 900000);
            userData.otp_code = sh1Pass;
            userData.otp_varified = false;
            userData.is_blocked = false;
            UserModels.registerUser(userData, function (signUpRes) {
                callback(signUpRes)
                if (signUpRes.status == "success") {
                    signUpRes.data.otp_code = sh1Pass;
                    callback(signUpRes)
                    // UserModels.setOTP(signUpRes.data, function (otpSet) {
                    //     if (loginInfo.status == "success") {
                    //         mailProperty('registerMail')(signUpRes.data.email, {
                    //             OTP: sh1Pass,
                    //             email: forgotPasswordData.email
                    //         }).send();

                    //         callback(loginInfo)
                    //     }
                    // })
                }
            })
        }
    },

    verifyCode: (user, callback) => {
        if (!user.email || typeof user.email === undefined) {
            callback({
                response_code: 400,
                response_data: {
                    status: "error",
                    message: "Please provide user email",
                    data: {}
                }

            });
        } else if (!user.otp_code || typeof user.otp_code === undefined) {
            callback({
                response_code: 400,
                response_data: {
                    "sttaus": "error",
                    "message": "Please provide OTP",
                    "data": {}
                }

            });

        } else {
            var token = jwt.sign({
                email: user.email
            }, authentication.secret, {
                expiresIn: authentication.tokenLife
            });

            var refreshToken = jwt.sign({
                email: user.email
            }, authentication.refreshTokenSecret, {
                expiresIn: authentication.refreshTokenLife
            });
            const response = {
                "status": "Logged in",
                "token": token,
                "refreshToken": refreshToken,
            }
            tokenList[refreshToken] = response
            user.auth_token = token
            UserModels.verifyCode(user, function (userData) {
                callback(userData)
            })
        }
    },

    completePatientProfile: (user, callback) => {

        UserModels.completePatientProfile(user, function (results) {

            callback(results)
        })

    },
    registerDoctor: (data, callback) => {
        data.is_blocked = true;
        UserModels.completeDoctorProfile(data, function (results) {
            callback(results)
        })
    },
    registerLaboratory: (data, callback) => {
        data.is_blocked = true;
        UserModels.completeLaboratoryProfile(data, function (results) {
            callback(results)
        })
    },

    updateProfilePatient: (user, callback) => {
        UserModels.completePatientProfile(user, function (results) {
            callback(results)
        })
    },

    updateProfileDoctor: (user, callback) => {
        UserModels.completeDoctorProfile(user, function (results) {
            callback(results)
        })
    },

    updateProfileLaboratory: (user, callback) => {
        UserModels.completeLaboratoryProfile(user, function (results) {
            callback(results)
        })
    },

    scheduleAppointment: (data, callback) => {
        BookingModels.scheduleAppointment(data, function (results) {
            callback(results)
        })
    },
    bookingDetails: (data, callback) => {
        BookingModels.bookingDetails(data, function (results) {
            callback(results)
        })
    },
    listBokkings: (data, callback) => {
        BookingModels.bookingList(data, function (results) {
            callback(results)
        })
    },
    providers: (data, callback) => {
        UserModels.providers(data, function (results) {
            callback(results)
        })
    },

    logout: (logoutData, callback) => {
        if (logoutData.user_id) {
            UserModels.logout(logoutData, function (logoutRes) {
                callback(logoutRes);
            })
        } else {
            callback({
                "status": "error",
                "message": "Insufficient information provided",
                "data": {}
            })
        }
    },
    changePassword: (data, callback) => {
        UserModels.changePassword(data, function (result) {
            callback(result);
        })
    },

    forgotPassword: (data, callback) => {
        if (!data.email || typeof data.email === undefined) {
            callback({
                "status": "error",
                "message": "Please provide email.",
                "data": {}
            })
        } else {
            UserModels.verifyUser(data, function (userData) {
                if (userData.status == 'success') {
                    var random = Math
                        .random()
                        .toString(36)
                        .replace(/[^a-z]+/g, '')
                        .substr(0, 6);
                    var sh1Pass = sha1(random);
                    console.log(sh1Pass);
                    UserModels.savePassword(data, sh1Pass, function (newPass) {
                        userData.random = random;


                        if (newPass.status === "success") {
                            mailProperty('forgotPasswordMail')(data.email, {
                                OTP: content.random,
                                email: data.email
                            }).send();
                            callback({
                                "status": "success",
                                "message": "New password will be sent to your mail.",
                                "data": {}
                            })
                        } else {
                            callback(newPass);
                        }
                    })
                } else {
                    callback(userData);
                }
            })
        }
    },
    search: (data, callback) => {
        UserModels.search(data, function (result) {
            callback(result)
        })
    },
    contents: (callback) => {
        ContentModels.contents(function (result) {
            callback(result)
        })
    },

    patientDetails: (data, callback) => {
        if (!data.id || typeof data.id === undefined) {
            callback({
                status: "error",
                message: "please provide id",
                data: {}
            })
        } else {
            UserModels.patientDetails(data, function (result) {
                callback(result)
            })
        }
    },
    providerDetails: (data, callback) => {
        if (!data.id || typeof data.id === undefined) {
            callback({
                status: "error",
                message: "please provide id",
                data: {}
            })
        } else {
            UserModels.providerDetails(data, function (result) {
                callback(result)
            })
        }
    },
    weeklyCalender: (data, callback) => {
        if (!data.id || typeof data.id === undefined) {
            callback({
                status: "error",
                message: "please provide id",
                data: {}
            })
        } else {
            UserModels.weeklyCalender(data, function (result) {
                callback(result)
            })
        }
    },
    addCountries: (callback) => {
        CountryModels.addCountries(function (result) {
            callback(result)
        })
    },
    countries: (callback) => {
        CountryModels.countries(function (result) {
            callback(result)
        })
    },
    testFacilities: (callback) => {
        callback({
            response_code: 200,
            response_data: {
                status: true,
                message: "Testing facilities",
                data: [{
                        name: 'X-ray',
                        price: "30$"
                    },
                    {
                        name: "Radiology",
                        price: '40$'
                    }
                ]
            }
        })
    }

}

module.exports = apiService;