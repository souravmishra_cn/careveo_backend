var jwt = require('jsonwebtoken');
var {
    authentication
} = require('../config');
module.exports = (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token']
    if (token) {
        jwt.verify(token, authentication.secret, function (err, decoded) {
            if (err) {
                return res.status(401).send({
                    status: false,
                    message: 'Unauthorized access.',
                    data: {}


                });
            }
            req.decoded = decoded;
            next();
        });
    } else {
        return res.status(404).send({
            status: false,
            message: 'No token provided',
            data: {}
        });
    }
}