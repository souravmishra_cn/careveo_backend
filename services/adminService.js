var express = require("express");

var config = require('../config');
var fs = require('fs');
var jwt = require('jsonwebtoken');
var secretKey = require('../config').secretKey;
var async = require("async");
var mongo = require('mongodb');
var crypto = require('crypto');
var sha1 = require('node-sha1');
// var mailProperty = require('../modules/sendMail');
var ObjectID = mongo.ObjectID;
var baseUrl = config.baseUrl;

var {
    authentication
} = require('../config');
var AdminModels = require('../models/admin');
var UserModels = require('../models/user');
var ContentModels = require('../models/content');
var adminService = {
    jwtAuthVerification: (token, callback) => {
        //  console.log(token);
        async.waterfall([
            function (nextCb) {
                if (token == null || token == undefined) {
                    nextCb({
                        success: false,
                        response_code: 5002,
                        response_message: "No token provided."
                    });
                } else {
                    nextCb(null, {
                        response_code: 2000
                    })
                }
            },
            function (arg1, nextCb) {
                if (arg1.response_code === 2000) {
                    jwt.verify(token, secretKey, function (err, decoded) {
                        // console.log(err);
                        //console.log(decoded);
                        if (err) {
                            nextCb(null, {
                                success: false,
                                response_code: 4002,
                                response_message: "Session timeout! Please login again.",
                                token: null
                            });
                        }
                        if (!err) {
                            nextCb(null, {
                                success: true,
                                response_code: 2000,
                                response_message: "Authenticate successfully.",
                                response_data: decoded
                            });
                        }
                    });
                }
            }
        ], function (err, content) {
            if (err) {
                callback({
                    "success": false,
                    "response_code": 5005,
                    "response_message": "INTERNAL DB ERROR",
                    "response_data": err
                })
            }
            if (content.response_code === 5005) {
                callback(content);
            }
            if (content.response_code === 4002) {
                callback(content);
            }
            if (content.response_code === 2000) {
                callback(content);
            }
        });
    },

    adminSignup: function (adminData, callback) {
        AdminModels.findOne({
            email: adminData.email
        }, function (err, admindet) {
            if (err)
                callback({
                    "status": "error",
                    "message": "Internal error"
                });
            else {
                if (admindet) {
                    // cError1 =;
                    callback({
                        "status": "error",
                        "message": "Email already taken",
                        "data": admindet
                    });
                } else {
                    adminData._id = new ObjectID;
                    var token = jwt.sign({
                        email: adminData.email
                    }, secretKey, {
                        //  expiresIn:'20m' // expires in 1 hours
                    });
                    adminData.authtoken = token;
                    AdminModels.registerUser(adminData, function (signUpRes) {
                        //nextCb(null, signUpRes); 
                        callback({
                            "status": "success",
                            "message": "Admin saved succefully",
                            "data": signUpRes
                        });
                    })
                }
            }
        });
    },
    adminLogin: (loginData, callback) => {
        if (!loginData.email || typeof loginData.email === undefined) {
            callback({
                "status": "error",
                "message": "please provide email",
                "data": {}
            });
        } else if (!loginData.password || typeof loginData.password === undefined) {
            callback({
                "status": "error",
                "message": "please provide password",
                "data": {}
            });
        } else {
            var token = jwt.sign({
                email: loginData.email
            }, authentication.secret, {
                expiresIn: authentication.tokenLife
            });
            console.log(token);
            loginData.authtoken = token;
            AdminModels.login(loginData, function (loginInfo) {
                loginInfo.token = token;
                callback({
                    "status": "success",
                    "message": "Admin logged in succfully",
                    "data": loginInfo
                });
            })
        }

    },
    createContent: (data, callback) => {
        if (!data.title || typeof data.title === undefined) {
            callback({
                "status": "error",
                "message": "please provide title",
                "data": {}
            });
        } else if (!data.page_french || typeof data.page_french === undefined) {
            callback({
                "status": "error",
                "message": "please provide page",
                "data": {}
            });
        } else if (!data.page_english || typeof data.page_english === undefined) {
            callback({
                "status": "error",
                "message": "please provide page",
                "data": {}
            });
        } else {
            ContentModels.createContent(data, function (content) {
                callback({
                    "status": "success",
                    "message": "New content created",
                    "data": content
                });
            })
        }

    },
    contents: (callback) => {
        ContentModels.contents(function (content) {
            callback({
                "status": "success",
                "message": "All contents",
                "data": content.response_data.data
            });
        })
    },
    fetchContent: (data, callback) => {
        if (!data.contentId || typeof data.contentId === undefined) {
            callback({
                "status": "error",
                "message": "please provide id",
                "data": {}
            });
        } else {
            ContentModels.fetchContent(data, function (content) {
                callback({
                    "status": "success",
                    "message": "Content",
                    "data": content
                });
            })
        }

    },
    deleteContent: (data, callback) => {
        if (!data.contentId || typeof data.contentId === undefined) {
            callback({
                "status": "error",
                "message": "please provide id",
                "data": {}
            });
        } else {
            ContentModels.deleteContent(data, function (content) {
                callback({
                    "status": "success",
                    "message": "Content",
                    "data": content.response_data.data
                });
            })
        }

    },
    updateContent: (data, callback) => {
        if (!data.id || typeof data.id === undefined) {
            callback({
                "status": "error",
                "message": "please provide id",
                "data": {}
            });
        }
        if (!data.title || typeof data.title === undefined) {
            callback({
                "status": "error",
                "message": "please provide title",
                "data": {}
            });
        } else if (!data.page_french || typeof data.page_french === undefined) {
            callback({
                "status": "error",
                "message": "please provide page",
                "data": {}
            });
        } else if (!data.page_english || typeof data.page_english === undefined) {
            callback({
                "status": "error",
                "message": "please provide page",
                "data": {}
            });
        } else {
            ContentModels.updateContent(data, function (content) {
                callback({
                    "status": "success",
                    "message": "Content Updated",
                    "data": content
                });
            })
        }
    },
    providers: (data, callback) => {
        UserModels.providers(data, function (results) {
            callback({
                "status": "success",
                "message": "All Doctors",
                "data": results.response_data.data
            });
        })
    }


};
module.exports = adminService;