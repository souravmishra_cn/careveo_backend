var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var async = require("async");
var mongo = require('mongodb');
var ObjectID = mongo.ObjectID;
var mongoosePaginate = require('mongoose-paginate');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

var countries = require('../countries.json');

// Export your module
var CountryModels = mongoose.model("Country", function () {
    var s = new mongoose.Schema({
        _id: {
            type: String,
            required: true
        },

        country_code: {
            type: String
        },
        country_name: {
            type: String,
            required: true
        }


    }, {
        timestamps: true
    });
    s.plugin(mongooseAggregatePaginate);
    s.plugin(mongoosePaginate);


    s.statics.addCountries = function (callback) {
        saveData = [];
        countries.map(async (country) => {
            country._id = new ObjectID
            await new CountryModels(country).save()
                .then((saved) => {
                    console.log('saveData', saveData.length)
                    console.log('countries', countries.length)
                    saveData.push(saved)
                    if (saveData.length == (countries.length - 1)) {
                        callback(saved)
                    }
                }).catch((e) => {
                    callback(e)
                })
        })

    }
    s.statics.countries = function (callback) {
        CountryModels.find({}, (err, countries) => {
            if (!err) {

                callback({
                    response_code: 200,
                    response_data: {
                        status: true,
                        message: "Countries List",
                        data: countries
                    }
                });
            } else {
                console.log(err);
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "Internal DB eror",
                        data: {}
                    }
                });
            }
        })
    }

    return s;

}());

module.exports = CountryModels;