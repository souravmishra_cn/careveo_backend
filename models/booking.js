var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var async = require("async");
var mongo = require('mongodb');
var jwt = require('jsonwebtoken');
var secretKey = require('../config').secretKey;
var ObjectID = mongo.ObjectID;
var mongoosePaginate = require('mongoose-paginate');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');


// Export your module
var BookingModels = mongoose.model("Booking", function () {
    var s = new mongoose.Schema({
        _id: {
            type: String,
            required: true
        },
        patient: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        provider: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        time_slot: {
            type: String,
            required: true
        },
        date: {
            type: String,
            required: true
        },
        existing_new: {
            type: String,
            enum: [
                "New", "Exsisting"
            ],
            default: "New"
        },
        age_group: {
            type: String,
            enum: [
                "10-15", "15-25", "25-40", "40-65"
            ],
            default: "25-40"
        },
        appointment_type: {
            type: String,
            enum: [
                "Online", "Offline"
            ],
            default: "Offline"
        },
        type_of_service: {
            type: String,
            default: ""
        },


        default: false
    }, {
        timestamps: true
    });
    s.plugin(mongooseAggregatePaginate);
    s.plugin(mongoosePaginate);


    s.statics.scheduleAppointment = function (data, callback) {
        if (data.id) {
            BookingModels.findByIdAndUpdate(data.id, data, {
                    new: 1
                })
                .populate('patient')
                .populate('provider')
                .exec(function (err, booking) {

                    if (err) {
                        // console.log('error....', err);
                        callback({
                            response_code: 500,
                            response_data: {
                                status: false,
                                message: "INTERNAL DB ERROR",
                                data: {}
                            }
                        })
                    }
                    if (booking) {
                        // console.log('userdata', userdata);

                        callback({
                            response_code: 200,
                            response_data: {
                                status: true,
                                message: "Appoint Rescheduled",
                                data: booking
                            }
                        })



                    } else {
                        callback({
                            response_code: 404,
                            response_data: {
                                "status": true,
                                "message": "Appoint not found does not exsist",
                                "data": {}
                            }
                        })
                    }
                })

        } else {
            console.log('request', data)
            let appointment = {
                _id: new ObjectID(),
                patient: data.patient,
                provider: data.provider,
                date: data.date,
                time_slot: data.time_slot,
                existing_new: data.existing_new,
                age_group: data.age_group,
                appointment_type: data.appointment_type
            };
            new BookingModels(appointment).save(function (err, booking) {
                if (!err) {
                    BookingModels.findOne({
                        _id: booking._id
                    }).populate('patient').populate('provider').exec((err, booked) => {
                        if (err) {
                            callback({
                                response_code: 500,
                                response_data: {
                                    status: false,
                                    message: "INTERNAL DB ERROR",
                                    data: {}
                                }
                            })
                        }
                        callback({
                            response_code: 200,
                            response_data: {
                                status: true,
                                message: "Your new appointment is scheduled",
                                data: booked
                            }
                        })
                    })


                } else {
                    console.log(err);
                    callback({
                        response_code: 500,
                        response_data: {
                            status: false,
                            message: "INTERNAL DB ERROR",
                            data: {}
                        }
                    })
                }
            })
        }
    }
    s.statics.bookingList = function (data, callback) {
        BookingModels.find({
            provider: data.provider
        }, (err, bookings) => {
            if (err) {
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "INTERNAL DB ERROR",
                        data: {}
                    }
                })
            }
            callback({
                response_code: 200,
                response_data: {
                    status: true,
                    message: "List of appointments",
                    data: bookings
                }
            })

        })
    }
    s.statics.bookingDetails = function (data, callback) {
        BookingModels.findOne({
                _id: data.id
            }).populate('patient')
            .populate('provider').exec((err, booking) => {
                if (err) {
                    callback({
                        response_code: 500,
                        response_data: {
                            status: false,
                            message: "INTERNAL DB ERROR",
                            data: {}
                        }
                    })
                }
                if (booking) {
                    callback({
                        response_code: 200,
                        response_data: {
                            status: true,
                            message: "Booking details",
                            data: booking
                        }
                    })

                } else {
                    callback({
                        response_code: 500,
                        response_data: {
                            status: false,
                            message: "INTERNAL DB ERROR",
                            data: {}
                        }
                    })
                }
            })
    }

    return s;

}());

module.exports = BookingModels;