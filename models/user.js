var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var async = require("async");
var mongo = require('mongodb');
var jwt = require('jsonwebtoken');
var {
    authentication
} = require('../config');
var ObjectID = mongo.ObjectID;
var mongoosePaginate = require('mongoose-paginate');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const {
    config
} = require("process");

// Export your module
var UserModels = mongoose.model("User", function () {
    var s = new mongoose.Schema({
        _id: {
            type: String,
            required: true
        },
        full_name: {
            type: String,
            required: true
        },
        email: {
            type: String,
            unique: true
        },
        address: {
            type: String,
            default: ''
        },
        age: {
            type: String,
            default: ''
        },
        country: {
            type: String,
            default: ''
        },
        location_lat: {
            type: Number,
            default: null
        },
        location_long: {
            type: Number,
            default: null
        },
        diploma: {
            type: String,
            default: ''
        },
        language: {
            type: String,
            default: ''
        },
        experiences: [{
            hospital_name: {
                type: String,
                default: ''
            },
            start_date: {
                type: String,
                default: ''
            },
            end_date: {
                type: String,
                default: ''
            },
            currently_employed: {
                type: Boolean,
                enum: [
                    true, false
                ],
                default: false
            }
        }],
        certificates: [{
            certificate_url: {
                type: String,
                default: ''
            }
        }],
        patient_speciality: [{
            name: {
                type: String,
                default: ''
            }
        }],
        specialization: {
            type: String,
            default: ''
        },
        works_publication: {
            type: String,
            default: ''
        },
        about: {
            type: String,
            default: ''
        },
        bio: {
            type: String,
            default: ''
        },
        bank_details: {
            bank_name: {
                type: String,
                default: ''
            },
            account_name: {
                type: String,
                default: ''
            },
            account_number: {
                type: String,
                default: ''
            },
            active: {
                type: Boolean,
                enum: [
                    true, false
                ],
                default: true
            }
        },
        resume_url: {
            type: String,
            default: ''
        },
        weekly_avalaibility: {
            sunday: {
                start_time: {
                    type: String,
                    default: ''
                },
                end_time: {
                    type: String,
                    default: ''
                },
                available: {
                    type: Boolean,
                    enum: [
                        true, false
                    ],
                    default: false
                }
            },
            monday: {
                start_time: {
                    type: String,
                    default: ''
                },
                end_time: {
                    type: String,
                    default: ''
                },
                available: {
                    type: Boolean,
                    enum: [
                        true, false
                    ],
                    default: false
                }
            },
            tuesday: {
                start_time: {
                    type: String,
                    default: ''
                },
                end_time: {
                    type: String,
                    default: ''
                },
                available: {
                    type: Boolean,
                    enum: [
                        true, false
                    ],
                    default: false
                }
            },
            wednesday: {
                start_time: {
                    type: String,
                    default: ''
                },
                end_time: {
                    type: String,
                    default: ''
                },
                available: {
                    type: Boolean,
                    enum: [
                        true, false
                    ],
                    default: false
                }
            },
            thursday: {
                start_time: {
                    type: String,
                    default: ''
                },
                end_time: {
                    type: String,
                    default: ''
                },
                available: {
                    type: Boolean,
                    enum: [
                        true, false
                    ],
                    default: false
                }
            },
            friday: {
                start_time: {
                    type: String,
                    default: ''
                },
                end_time: {
                    type: String,
                    default: ''
                },
                available: {
                    type: Boolean,
                    enum: [
                        true, false
                    ],
                    default: false
                }
            },
            staurday: {
                start_time: {
                    type: String,
                    default: ''
                },
                end_time: {
                    type: String,
                    default: ''
                },
                available: {
                    type: Boolean,
                    enum: [
                        true, false
                    ],
                    default: false
                }
            }
        },
        testing_facilities: [{
            name: {
                type: String,
                default: ''
            },
            price: {
                type: String,
                default: ''
            }
        }],
        education: {
            type: String,
            default: ''
        },
        health_insurance: {
            type: String,
            default: ''
        },
        medical_condition: {
            type: String,
            default: ''
        },
        problem_description: {
            type: String,
            default: ''
        },
        problem_summary: {
            type: String,
            default: ''
        },
        patients_documets: [{
            doc_url: {
                type: String,
                default: ''
            }
        }],
        pun: {
            type: String,
            default: ''
        },
        laboratory: {
            type: String,
            default: ''
        },
        radiology: [{
            name: {
                type: String,
                default: ''
            }
        }],
        allergy: {
            type: String,
            default: ''
        },
        password: {
            type: String,
            required: true
        },
        gender: {
            type: String,
            enum: [
                'Male', 'Female', 'Other'
            ],
            default: 'Male'
        },
        dob: {
            type: String,
            default: ''
        },
        auth_token: {
            type: String,
            default: ''
        },
        user_type: {
            type: String,
            enum: [
                'Patient', 'Doctor', 'Laboratory'
            ],
            default: 'Patient',
            required: true
        },
        phone: {
            type: String,
            default: ''
        },
        image_url: {
            type: String,
            default: ''
        },
        is_blocked: {
            type: Boolean,
            default: false
        },
        otp_code: {
            type: String,
            defaul: ''
        },
        otp_varified: {
            type: Boolean,
            defaul: false
        },
        is_loggedIn: {
            type: Boolean,
            defaul: false
        },
        default: false
    }, {
        timestamps: true
    });
    s.plugin(mongooseAggregatePaginate);
    s.plugin(mongoosePaginate);

    s.pre('save', function (next) {
        var user = this;
        if (!user.isModified('password'))
            return next();

        bcrypt.hash(user.password, null, null, function (err, hash) {
            if (err) {
                return next(err);
            }

            user.password = hash;
            next();
        });
    });
    s.statics.registerUser = function (user, callback) {
        this.getUserByEmail(user.email, function (res) {
            console.log("res", user);
            if (res) {
                console.log("[user already registered]");
                callback({
                    response_code: 302,
                    response_data: {
                        status: false,
                        message: "User already exsist.",
                        data: {}
                    }
                })
            } else {
                user._id = new ObjectID
                new UserModels(user).save(function (err, result) {
                    if (!err) {
                        console.log("[registered user]");
                        callback({
                            response_code: 200,
                            response_data: {
                                status: true,
                                message: "User registered succefully",
                                data: {
                                    full_name: result.full_name,
                                    email: result.email,
                                    phone: result.phone,

                                }
                            }
                        })
                    } else {
                        console.log(err);
                        callback({
                            response_code: 500,
                            response_data: {
                                status: false,
                                message: "Internal DB error",
                                data: {}
                            }

                        })
                    }
                })


            }
        })
    }
    s.statics.login = function (loginData, callback) {

        UserModels.findOne({
            email: loginData.email
        }, function (err, profileRes) {
            if (err) {
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "INTERNAL DB ERROR",
                        data: {}
                    }
                })
            }
            console.log(profileRes);
            if (profileRes) {
                //console.log(profileRes);

                var p = UserModels.comparePassword(loginData.password, profileRes.password);
                if (p === true) {
                    UserModels.update({
                        _id: profileRes._id
                    }, {
                        $set: {
                            auth_token: loginData.auth_token,
                            is_loggedIn: true

                        }
                    }).exec(function (err, us) {
                        if (us.n === 1 && us.nModified === 1) {
                            callback({
                                response_code: 200,
                                response_data: {
                                    status: true,
                                    message: "Login successful",
                                    data: {
                                        _id: profileRes._id,
                                        auth_token: loginData.auth_token,
                                        user_type: profileRes.user_type
                                    }
                                }
                            })


                        }
                    })


                }
                if (p === false) {
                    callback({
                        response_code: 400,
                        response_data: {
                            status: false,
                            message: "Either email/password is wrong",
                            data: {}
                        }
                    })
                }
            }
            if (profileRes === null) {
                callback({
                    response_code: 404,
                    response_data: {
                        status: false,
                        message: "No user found",
                        data: {}
                    }
                })

            }
        })
    }
    s.statics.verifyCode = function (user, callback) {
        this.getUserByEmail(user.email, function (profileData) {
            console.log('profileData', profileData);
            if (profileData) {

                if (profileData.otp_varified == false) {
                    if (parseInt(user.otp_code) == profileData.otp_code) {
                        let newUser = profileData;
                        newUser.otp_varified = true;
                        newUser.auth_token = user.auth_token
                        UserModels.findByIdAndUpdate(profileData._id, newUser, {
                            new: 1
                        }, function (err, userdata) {

                            console.log('user data', userdata);
                            console.log('user error', err);
                            if (err) {
                                // console.log('error....', err);
                                callback({
                                    response_code: 500,
                                    response_data: {
                                        status: false,
                                        message: "INTERNAL DB ERROR",
                                        data: {}
                                    }
                                })
                            }
                            if (userdata) {

                                callback({
                                    response_code: 200,
                                    response_data: {
                                        status: true,
                                        message: "Token tverified succesfully",
                                        data: {
                                            id: userdata._id,
                                            auth_token: userdata.auth_token,
                                            user_type: userdata.user_type
                                        }
                                    }
                                })


                            } else {
                                callback({
                                    response_code: 404,
                                    response_data: {
                                        status: false,
                                        message: "User does not exsist",
                                        data: {}
                                    }
                                })
                            }
                        })



                    } else {
                        callback({
                            response_code: 404,
                            response_data: {
                                status: false,
                                message: "Invalid OTP!",
                                data: {}
                            }
                        })

                    }
                } else {
                    callback({
                        response_code: 200,
                        response_data: {
                            status: true,
                            message: "OTP already varified.",
                            data: {}
                        }
                    })

                }
            }
        })
    }
    s.statics.logout = function (logoutData, callback) {


        //deviceToken.push(logoutData.devicetoken);
        var conditions = {
            _id: logoutData.user_id
        };

        let fields = {
            auth_token: '',
            is_loggedIn: false

        };

        options = {
            upsert: false
        };
        UserModels.update(conditions, fields, options, function (err, affected) {
            // console.log(err); console.log(affected);
            if (err) {
                callback({
                    response_code: 500,
                    response_data: {
                        status: "error",
                        message: "INTERNAL DB ERROR",
                        data: {}
                    }
                })


            } else {
                callback({
                    response_code: 200,
                    response_data: {
                        status: true,
                        message: "You logged out successfully.",
                        data: {}
                    }
                })

            }
        });
    }

    s.statics.changePassword = function (passData, callback) {
        bcrypt.hash(passData.password, null, null, function (err, hash) {
            if (err) {
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "INTERNAL DB ERROR",
                        data: {}
                    }
                })
            }
            if (!err) {
                var newPassword = hash;
                //console.log(newPassword);
                UserModels
                    .update({
                        email: passData.email
                    }, {
                        $set: {
                            password: newPassword
                        }
                    })
                    .exec(function (err, updated) {
                        if (err) {
                            callback({
                                response_code: 500,
                                response_data: {
                                    status: false,
                                    message: "INTERNAL DB ERROR",
                                    data: {}
                                }
                            })
                        }
                        if (!err) {
                            if (updated.n === 1 && updated.nModified === 1) {
                                callback({
                                    response_code: 200,
                                    response_data: {
                                        status: true,
                                        message: "Password saved succesfully.",
                                        data: {}
                                    }
                                })

                            }
                        }
                    })
            }
        });
    }

    s.statics.completeProfile = function (data, callback) {
        UserModels.getUserByUserId(data.id, function (found) {
            if (found.status == true) {
                if (data.user_type == 'Patient') {
                    UserModels.completePatientProfile(data, function (completed) {
                        callback(completed)
                    })

                } else if (data.user_type == 'Doctor') {
                    UserModels.completeDoctorProfile(data, function (completed) {
                        callback(completed)
                    })
                } else if (data.user_type == 'Laboratory') {
                    UserModels.completeLaboratoryProfile(data, function (completed) {
                        callback(completed)
                    })
                } else {
                    callback({
                        response_code: 400,
                        response_data: {
                            status: false,
                            message: "Sorry...insuficient data",
                            data: {}
                        }
                    })
                }
            } else {
                callback(found)
            }
        })

    }
    s.statics.completePatientProfile = function (data, callback) {
        console.log('completePatientProfile', data)
        UserModels.findById(data.id, (err, user) => {
            if (err) {
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "INTERNAL DB ERROR",
                        data: {}
                    }
                })
            }
            data.patients_documets.map(doc => {
                doc._id = new ObjectID();
                user.patients_documets.push(doc)
            })
            data.patients_documets = user.patients_documets;
            UserModels.findByIdAndUpdate(data.id, data, {
                new: 1
            }, function (err, userdata) {

                console.log('user data', userdata);
                console.log('user error', err);
                if (err) {
                    // console.log('error....', err);
                    callback({
                        response_code: 500,
                        response_data: {
                            status: false,
                            message: "INTERNAL DB ERROR",
                            data: {}
                        }
                    })
                }
                if (userdata) {
                    // console.log('userdata', userdata);

                    callback({
                        response_code: 200,
                        response_data: {
                            status: true,
                            message: "Profile Updated Successfully",
                            data: {
                                _id: userdata._id,
                                full_name: userdata.full_name,
                                address: userdata.address,
                                phone: userdata.userdata,
                                age: userdata.age,
                                dob: userdata.userdata,
                                email: userdata.email,
                                user_type: userdata.user_type,
                                patient_speciality: userdata.patient_speciality,
                                medical_condition: userdata.medical_condition,
                                laboratory: userdata.laboratory,
                                radiology: userdata.radiology,
                                allergy: userdata.allergy,
                                problem_description: userdata.problem_description,
                                problem_summary: userdata.problem_summary,
                                age: userdata.age,
                                patients_documets: userdata.patients_documets,
                                pun: userdata.pun,
                                health_insurance: userdata.health_insurance,
                                gender: userdata.gender,
                                image_url: userdata.image_url,
                                location_lat: userdata.location_lat,
                                location_long: userdata.location_long
                            }
                        }
                    })



                } else {
                    callback({
                        response_code: 404,
                        response_data: {
                            status: false,
                            message: "User does not exsist",
                            data: {}
                        }
                    })
                }
            })
        })

    }
    s.statics.completeDoctorProfile = function (data, callback) {
        console.log('completeDoctorProfile', data);
        if (data.id) {
            UserModels.findByIdAndUpdate(data.id, data, {
                new: 1
            }, function (err, saved) {

                //console.log('user data', userdata); console.log('user error', err);
                if (err) {

                    callback({
                        response_code: 500,
                        response_data: {
                            status: false,
                            message: "INTERNAL DB ERROR",
                            data: err
                        }
                    })
                }
                if (saved) {
                    // console.log(7777, userdata);
                    callback({
                        response_code: 200,
                        response_data: {
                            status: true,
                            message: "Profile Updated Successfully",
                            data: {
                                id: saved._id,
                                user_type: saved.user_type,
                                age: saved.age,
                                phone: saved.phone,
                                email: saved.email,
                                address: saved.address,
                                full_name: saved.full_name,
                                gender: saved.gender,
                                certificates: saved.certificates,
                                diploma: saved.diploma,
                                language: saved.language,
                                works_publication: saved.works_publication,
                                bio: saved.bio,
                                experiences: saved.experiences,
                                bank_details: saved.bank_details,
                                education: saved.education,
                                weekly_avalaibility: saved.weekly_avalaibility,
                                gender: saved.gender,
                                image_url: saved.image_url,
                                location_lat: saved.location_lat,
                                location_long: saved.location_long
                            }
                        }
                    })

                } else {
                    callback({
                        response_code: 404,
                        response_data: {
                            status: false,
                            message: "User does not exsist",
                            data: {}
                        }
                    })
                }
            })
            // UserModels.findOne({
            //     _id: data.id
            // }, (err, user) => {
            //     if (user) {
            //         user = data


            //         user.save((err, saved) => {
            //             if (saved) {
            //                 callback({
            //                     response_code: 200,
            //                     response_data: {
            //                         status: true,
            //                         message: "Profile Updated Successfully",
            //                         data: {
            //                             id: saved._id,
            //                             user_type: saved.user_type,
            //                             age: saved.age,
            //                             phone: saved.phone,
            //                             email: saved.email,
            //                             address: saved.address,
            //                             full_name: saved.full_name,
            //                             gender: saved.gender,
            //                             certificates: saved.certificates,
            //                             diploma: saved.diploma,
            //                             language: saved.language,
            //                             works_publication: saved.works_publication,
            //                             bio: saved.bio,
            //                             experiences: saved.experiences,
            //                             bank_details: saved.bank_details,
            //                             education: saved.education
            //                         }
            //                     }
            //                 })

            //             }
            //             if (err) {
            //                 callback({
            //                     response_code: 500,
            //                     response_data: {
            //                         status: false,
            //                         message: "INTERNAL DB ERROR",
            //                         data: err
            //                     }
            //                 })
            //             }
            //         })

            //     } else {
            //         callback({
            //             response_code: 404,
            //             response_data: {
            //                 status: true,
            //                 message: "User does not exsist",
            //                 data: {}
            //             }
            //         })

            //     }
            //     if (err) {
            //         callback({
            //             response_code: 500,
            //             response_data: {
            //                 status: false,
            //                 message: "INTERNAL DB ERROR",
            //                 data: err
            //             }
            //         })
            //     }
            // })
        } else {
            data._id = new ObjectID;
            new UserModels(data).save(function (err, result) {
                if (!err) {
                    console.log("[registered user]");
                    callback({
                        response_code: 200,
                        response_data: {
                            status: true,
                            message: "Doctor registered succefully",
                            data: {
                                id: result._id,
                                user_type: result.user_type,
                                age: result.age,
                                phone: result.phone,
                                email: result.email,
                                address: result.address,
                                full_name: result.full_name,
                                gender: result.gender,
                                certificates: result.certificates,
                                diploma: result.diploma,
                                language: result.language,
                                works_publication: result.works_publication,
                                bio: result.bio,
                                experiences: result.experiences,
                                bank_details: result.bank_details,
                                education: result.education,
                                gender: result.gender,
                                image_url: result.image_url,
                                location_lat: result.location_lat,
                                location_long: result.location_long
                            }
                        }
                    })

                } else {
                    console.log(err);
                    callback({
                        response_code: 500,
                        response_data: {
                            status: false,
                            message: "INTERNAL DB ERROR",
                            data: err
                        }
                    })
                }
            })

        }


    }
    s.statics.completeLaboratoryProfile = function (data, callback) {
        if (data.id) {
            UserModels.findByIdAndUpdate(data.id, data, {
                new: 1
            }, function (err, userdata) {

                //console.log('user data', userdata); console.log('user error', err);
                if (err) {

                    callback({
                        response_code: 500,
                        response_data: {
                            status: false,
                            message: "INTERNAL DB ERROR",
                            data: err
                        }
                    })
                }
                if (userdata) {
                    // console.log(7777, userdata);
                    callback({
                        response_code: 200,
                        response_data: {
                            status: true,
                            message: "Profile Updated Successfully",
                            data: {
                                _id: userdata._id,
                                user_type: userdata.user_type,
                                image_url: userdata.image_url,
                                phone: userdata.phone,
                                email: userdata.email,
                                address: userdata.address,
                                full_name: userdata.full_name,
                                testing_facilities: userdata.testing_facilities,
                                bank_details: userdata.bank_details,
                                weekly_avalaibility: userdata.weekly_avalaibility,
                                gender: userdata.gender,
                                location_lat: userdata.location_lat,
                                location_long: userdata.location_long
                            }
                        }
                    })

                } else {
                    callback({
                        response_code: 404,
                        response_data: {
                            status: false,
                            message: "User does not exsist",
                            data: {}
                        }
                    })
                }
            })

        } else {


            data._id = new ObjectID;
            new UserModels(data).save(function (err, result) {
                if (!err) {
                    console.log("[registered user]");
                    callback({
                        response_code: 200,
                        response_data: {
                            status: true,
                            message: "Laboratory registered succefully",
                            data: {
                                _id: data._id,
                                user_type: data.user_type,
                                image_url: data.image_url,
                                phone: data.phone,
                                email: data.email,
                                address: data.address,
                                full_name: data.full_name,
                                testing_facilities: data.testing_facilities,
                                gender: data.gender
                            }
                        }
                    })
                } else {
                    console.log(err);
                    callback({
                        response_code: 500,
                        response_data: {
                            status: false,
                            message: "Internal DB error",
                            data: {}
                        }
                    })
                }
            })
        }

    }
    s.statics.providers = function (data, callback) {
        let query = {};
        if (data.user_type == 'Doctor') {
            query.user_type = data.user_type
        }
        if (data.user_type == 'Laboratory') {
            query.user_type = data.user_type
        }
        UserModels.find(query, (err, providers) => {
            if (err) {
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "INTERNAL DB ERROR",
                        data: {}
                    }
                })

            } else if (providers.length > 0) {
                let providers_array = []
                providers.map(provider => {
                    if (data.user_type == 'Doctor') {
                        providers_array.push({
                            id: provider._id,
                            user_type: provider.user_type,
                            phone: provider.phone,
                            email: provider.email,
                            full_name: provider.full_name
                        })
                    }
                    if (data.user_type == 'Laboratory') {
                        providers_array.push({
                            id: provider._id,
                            user_type: provider.user_type,
                            phone: provider.phone,
                            email: provider.email,
                            full_name: provider.full_name
                        })
                    }
                })
                callback({
                    response_code: 200,
                    response_data: {
                        status: true,
                        message: "Providers list",
                        data: providers_array
                    }
                })

            } else {
                callback({
                    response_code: 404,
                    response_data: {
                        "status": false,
                        "message": "Providers not found",
                        "data": {}
                    }
                })
            }
        })
    }

    s.statics.search = function (data, callback) {

        UserModels.find({
            $or: [{
                $or: [{
                        specialization: {
                            $regex: ".*" + data.specialization + ".*",
                            $options: 'i'
                        }
                    },
                    {
                        address: {
                            $regex: '.*' + data.location + '.*',
                            $options: 'i'
                        }
                    }
                ]
            }, {
                $or: [{
                    testing_facilities: {
                        $elemMatch: {
                            name: {
                                $regex: ".*" + data.name + ".*",
                                $options: 'i'
                            }
                        }
                    }
                }, {
                    address: {
                        $regex: '.*' + data.location + '.*',
                        $options: 'i'
                    }
                }]
            }]



        }, (err, providers) => {
            console.log('pidhbd', providers)
            if (err) {
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "INTERNAL DB ERROR",
                        data: {}
                    }
                })

            } else if (providers.length > 0) {

                callback({
                    response_code: 200,
                    response_data: {
                        status: true,
                        message: "Providers list",
                        data: providers
                    }
                })

            } else {
                callback({
                    response_code: 404,
                    response_data: {
                        status: false,
                        message: "Providers not found",
                        data: {}
                    }
                })
            }
        })
    }
    s.statics.patientDetails = function (data, callback) {
        UserModels.findOne({
            _id: data.id,
            user_type: "Patient"
        }, (err, userdata) => {
            if (err) {
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "INTERNAL DB ERROR",
                        data: {}
                    }
                })

            } else if (userdata) {
                callback({
                    response_code: 200,
                    response_data: {
                        status: true,
                        message: "User details",
                        data: {
                            _id: userdata._id,
                            full_name: userdata.full_name,
                            address: userdata.address,
                            phone: userdata.phone,
                            age: userdata.age,
                            dob: userdata.dob,
                            email: userdata.email,
                            user_type: userdata.user_type,
                            patient_speciality: userdata.patient_speciality,
                            medical_condition: userdata.medical_condition,
                            laboratory: userdata.laboratory,
                            radiology: userdata.radiology,
                            allergy: userdata.allergy,
                            problem_description: userdata.problem_description,
                            problem_summary: userdata.problem_summary,
                            age: userdata.age,
                            patients_documets: userdata.patients_documets,
                            pun: userdata.pun,
                            health_insurance: userdata.health_insurance,
                            gender: userdata.gender,
                            image_url: userdata.image_url,
                            location_lat: userdata.location_lat,
                            location_long: userdata.location_long
                        }
                    }
                })

            } else {
                callback({
                    response_code: 404,
                    response_data: {
                        status: false,
                        message: "User not found",
                        data: {}
                    }
                })
            }
        })
    }
    s.statics.providerDetails = function (data, callback) {
        UserModels.findOne({
            _id: data.id
        }, (err, provider) => {
            if (err) {
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "INTERNAL DB ERROR",
                        data: {}
                    }
                })

            } else if (provider) {
                if (provider.user_type == 'Doctor') {
                    callback({
                        response_code: 200,
                        response_data: {
                            status: true,
                            message: "Doctor details",
                            data: {
                                id: provider._id,
                                user_type: provider.user_type,
                                age: provider.age,
                                phone: provider.phone,
                                email: provider.email,
                                address: provider.address,
                                full_name: provider.full_name,
                                gender: provider.gender,
                                certificates: provider.certificates,
                                diploma: provider.diploma,
                                language: provider.language,
                                works_publication: provider.works_publication,
                                bio: provider.bio,
                                experiences: provider.experiences,
                                image_url: provider.image_url,
                                weekly_avalaibility: provider.weekly_avalaibility,
                                bank_details: provider.bank_details,
                                location_lat: provider.location_lat,
                                location_long: provider.location_long
                            }
                        }
                    })
                }
                if (provider.user_type == 'Laboratory') {
                    callback({
                        response_code: 200,
                        response_data: {
                            status: true,
                            message: "Laboratory details",
                            data: {
                                id: provider._id,
                                user_type: provider.user_type,
                                image_url: provider.image_url,
                                phone: provider.phone,
                                email: provider.email,
                                address: provider.address,
                                full_name: provider.full_name,
                                testing_facilities: provider.testing_facilities,
                                weekly_avalaibility: provider.weekly_avalaibility,
                                bank_details: provider.bank_details,
                                gender: provider.gender,
                                image_url: provider.image_url,
                                location_lat: provider.location_lat,
                                location_long: provider.location_long
                            }
                        }
                    })
                }
            } else {
                callback({
                    response_code: 404,
                    response_data: {
                        status: false,
                        message: "User not found",
                        data: {}
                    }
                })
            }
        })
    }
    s.statics.weeklyCalender = function (data, callback) {
        UserModels.findOne({
            _id: data.id
        }, (err, provider) => {
            if (err) {
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "INTERNAL DB ERROR",
                        data: {}
                    }
                })

            } else if (provider) {
                callback({
                    response_code: 200,
                    response_data: {
                        status: true,
                        message: "Weekly calender of provider",
                        data: provider.weekly_avalaibility
                    }
                })
            } else {
                callback({
                    response_code: 404,
                    response_data: {
                        status: false,
                        message: "User not found",
                        data: {}
                    }
                })
            }
        })
    }


    /**
     * New API End here
     * @param {*} data 
     * @param {*} callback 
     */
    s.statics.getUserDetails = function (data, callback) {
        UserModels.findById(data.id, function (err, user) {
            if (!err) {
                if (user) {
                    callback({
                        response_code: 200,
                        response_data: {
                            status: true,
                            message: "Your profile has been updated successfully",
                            data: user
                        }
                    })

                } else {
                    callback({
                        response_code: 404,
                        response_data: {
                            status: false,
                            message: "Use does not exsist",
                            data: {}
                        }
                    })



                }
            } else {
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "INTERNAL DB ERROR",
                        data: {}
                    }
                })


            }
        })
    }
    s.statics.updateUserProfile = function (user, callback) {
        //callback(555);
        console.log("User Model", user);
        UserModels.findByIdAndUpdate(user.user_id, user, {
            new: 1
        }, function (err, userdata) {

            //console.log('user data', userdata); console.log('user error', err);
            if (err) {
                console.log('error....', err);
                callback({
                    "response_code": 5005,
                    "response_message": "INTERNAL DB ERROR",
                    "response_data": err
                })
            }
            if (userdata) {
                console.log(7777, userdata);
                callback({
                    "response_code": 2000,
                    "response_message": "Profile Updated Successfully",
                    "response_data": userdata
                });

            } else {
                callback({
                    "response_code": 5005,
                    "response_message": "User Not Found"
                })
            }
        })
    }
    s.statics.getUserByEmail = function (email, callback) {
        UserModels.findOne({
            email: email
        }, function (err, res) {
            if (err)
                console.log(err);
            if (!err)
                callback(res);
        })
    }
    s.statics.getUserByUserId = function (userId, callback) {
        //console.log(userId);
        UserModels.findOne({
            _id: userId
        }, function (err, res) {
            //       console.log(res)
            if (err)
                callback({
                    status: false,
                    messgae: "Internal DB error",
                    data: {}
                })
            if (!err)
                callback({
                    status: true,
                    messgae: "user found",
                    data: res
                })
        })
    }

    s.statics.getProfileDetails = function (user_id, callback) {
        console.log('user_id', user_id)
        if (user_id) {
            UserModels.findOne({
                _id: user_id
            }, function (err, u) {
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": {}
                    })
                }
                if (!err) {
                    callback(u);
                }
            })
        }
    }

    s.statics.setOTP = function (reqData, callback) {
        //console.log(newPassword);
        UserModels
            .update({
                email: reqData.email
            }, {
                $set: {
                    otp_code: reqData.otp_code,
                    otp_verified: false
                }
            })
            .exec(function (err, updated) {
                if (err) {
                    callback({
                        "status": false,
                        "message": "INTERNAL DB ERROR",
                        "data": {}
                    })
                }
                if (!err) {
                    if (updated.n === 1 && updated.nModified === 1) {
                        callback({
                            "status": true,
                            "message": "OTP saved successfully.",
                            "data": {}
                        })
                    }
                }
            })
    }
    s.statics.comparePassword = function (password, dbPassword) {
        //console.log(password);
        return bcrypt.compareSync(password, dbPassword);
    }
    s.statics.authenticate = function (jwtData, callback) {
        jwt.verify(jwtData, authentication.secret, (err, decoded) => {
            if (err) {
                callback(err)
            }
            callback(decoded)
        })
    }
    s.statics.getUsers = function (userData, callback) {
        console.log(userData)
        var search = []
        if (userData.name) {
            search.push({
                name: {
                    $regex: ".*" + userData.name + ".*",
                    $options: 'i'
                }
            })
        }
        if (userData.email) {
            search.push({
                email_address: {
                    $regex: ".*" + userData.email + ".*",
                    $options: 'i'
                }
            })
        }
        if (userData.skill) {
            search.push({
                skills: {
                    $regex: ".*" + userData.skill + ".*",
                    $options: 'i'
                }
            })
        }
        if (userData.level) {
            search.push({
                level: {
                    $regex: ".*" + userData.level + ".*",
                    $options: 'i'
                }
            })
        }
        // UserModels.aggregate([     {         $match:{                 $and:search }
        // },     {         $project:{             name:1, email_address:1,
        // mobile_no:1,             total_exp:1, skills:1,             level:1,
        // image_url:1         }     } ]).exec( console.log(search.length);
        let keyword = search.length > 0 ?
            search : [{
                isBlocked: false
            }];
        console.log("sekeyword", keyword);
        UserModels.find({
            $and: keyword
        }, function (err, res) {
            if (err) {
                callback({
                    "response_code": 5005,
                    "response_message": "INTERNAL DB ERROR",
                    "response_data": {}
                })
            }
            if (!err) {
                // callback(res);
                callback({
                    "response_code": 2000,
                    "response_message": "success",
                    "response_data": res
                });
            }
        })
    }

    s.statics.getUsersAdmin1 = function (userData, callback) {
        var aggregate = UserModels.aggregate([{
            $lookup: {
                from: "reportusers",
                localField: "_id",
                foreignField: "reportuser_id",
                as: "reportuser"
            }
        }, {
            $project: {

                name: 1,
                username: 1,
                email: 1,
                gender: 1,
                aboutme: 1,
                user_type: 1,
                image_url: 1,
                isBlocked: 1,
                createdAt: 1,
                reportCount: {
                    $size: "$reportuser"
                }
            }
        }])
        var options = {

        };
        UserModels.aggregatePaginate(
            aggregate,
            options,
            function (err, results, pageCount, count) {
                if (err) {
                    console.log(err)
                    callback({
                        "success": false,
                        "response_code": 5000,
                        "response_message": "INTERNAL DB ERROR",
                        "total_record": 0,
                        "response_data": {}
                    })
                } else {
                    //console.log(results.length);
                    if (results.length == 0) {
                        callback({
                            "success": true,
                            "response_code": 5002,
                            "total_record": 0,
                            "response_message": "No user found",
                            "response_data": []
                        })
                    } else {
                        console.log('kjguhde', results);

                        callback({
                            "success": true,
                            "response_code": 2000,
                            "response_message": "New user list.",
                            "total_record": count,
                            "response_data": results
                        })
                    }

                }
            }
        ) //End pagination
    }
    s.statics.getUserList = function (userData, callback) {
        // console.log(userData);
        BlockuserModel.blockList(userData, function (blockres) {
            // console.log( "userIdArr",  userData.userIdArr) console.log("blockres",
            // blockres.response_data)
            if (blockres.response_data.length > 0) {
                userData.userIdArr = userData
                    .userIdArr
                    .filter(val => !blockres.response_data.includes(val));
            }
            //  console.log(userData.userIdArr);
            var aggregate = UserModels.aggregate([{
                $match: {
                    _id: {
                        $in: userData.userIdArr
                    }
                }
            }, {
                $project: {
                    name: 1,
                    user_type: 1,
                    email: 1,
                    image_url: 1,
                    gender: 1,
                    is_following: ""
                }
            }])
            //start pagination
            var options = {
                page: userData.page,
                limit: 10
            };
            UserModels.aggregatePaginate(
                aggregate,
                options,
                function (err, results, pageCount, count) {
                    if (err) {
                        console.log(err)
                    } else {
                        //console.log(results.length);
                        if (results.length == 0) {
                            callback({
                                "response_code": 5002,
                                "total_record": 0,
                                "response_message": "No user found",
                                "response_data": []
                            })
                        } else {
                            callback({
                                "response_code": 2000,
                                "response_message": "New user list.",
                                "total_record": count,
                                "response_data": results
                            })
                        }

                    }
                }
            ) //End pagination

        })
    }
    s.statics.getUserProfile = function (userData, callback) {
        // console.log(userData);
        var findwith = userData.user_id;

        if (findwith) {
            UserModels.findOne({
                _id: findwith
            }, function (err, profileRes) {
                if (err) {
                    callback({
                        "response_code": 5005,
                        "response_message": "INTERNAL DB ERROR",
                        "response_data": {}
                    })
                }
                if (!err) {
                    //console.log(profileRes);
                    if (profileRes != null) {
                        async.parallel({
                            user: function (callback) {
                                callback(null, profileRes);
                            },
                            // following: function(callback){
                            // FollowersModel.getFollowingUserList({userId:findwith}, function
                            // (followingRes) {     console.log('following');     console.log(followingRes);
                            // if(followingRes.response_code===2000)         {            callback(null,{
                            // "following_count":followingRes.total             })        }else{
                            // callback(null,{                 "following_count":0             })        }
                            // }) }, followers: function(callback){
                            // FollowersModel.getFollowerUserList({userId:findwith}, function (followerRes)
                            // {         console.log('followers');         console.log(followerRes); let
                            // follower_users=followerRes.response_data;         let
                            // arrindex=follower_users.indexOf(userData.user_id); console.log(arrindex)
                            // if(followerRes.response_code===2000)         { callback(null,{
                            // "followers_count":followerRes.total, "is_following":arrindex>-1?true:false
                            // })        }else{ callback(null,{                 "followers_count":0,
                            // "is_following":arrindex>-1?true:false             })        }    }) },

                        }, function (err, results) {
                            // results is now equals to: {one: 1, two: 2}
                            var userdetails = {
                                "response_code": 2000,
                                "response_message": "Success",
                                "response_data": {
                                    "authtoken": results.user.authtoken,
                                    "profile_type": results.user.user_type,
                                    "profile_details": {
                                        "user_id": results.user._id,
                                        "name": results.user.name,
                                        "username": results.user.username,
                                        "devicetoken": results.user.devicetoken,
                                        "apptype": results.user.apptype,
                                        "email": results.user.email,
                                        "gender": results.user.gender,
                                        "dob": results.user.dob,
                                        "profile_pic": results.user.image_url
                                    }
                                }
                            }
                            callback(userdetails);
                        });
                    } else {
                        callback({
                            "response_code": 5002,
                            "response_message": "No user found",
                            "response_data": {}
                        })
                    }
                }
            })
        } else {
            callback({
                "response_code": 5005,
                "response_message": "INTERNAL DB ERROR",
                "response_data": {}
            })
        }
    }
    s.statics.verifyUser = function (userData, callback) {
        //console.log(userData);
        UserModels.find({
            email: userData.email
        }, function (err, res) {
            // console.log(res);
            if (err) {
                callback({
                    "status": false,
                    "message": "INTERNAL DB ERROR",
                    "data": {}
                })
            } else if (res.length > 0) {
                callback({
                    "status": true,
                    "message": "User exsist",
                    "data": res
                })
            } else {
                callback({
                    "status": false,
                    "message": "User does not exsist exsist",
                    "data": {}
                })
            }
        })
    }
    s.statics.verifyOTP = function (resetPasswordData, callback) {
        console.log('resetPasswordData', resetPasswordData);
        UserModels.getUserByEmail(resetPasswordData.email, function (profileData) {
            console.log('profileData', profileData);
            if (profileData) {
                // var p = UserModels.comparePassword(resetPasswordData.password,
                // profileData.password);
                if (profileData.otp_verified == false) {
                    if (parseInt(resetPasswordData.otp) === profileData.otp) {
                        var token = crypto
                            .randomBytes(32)
                            .toString('hex');
                        UserModels
                            .update({
                                _id: profileData._id
                            }, {
                                $set: {
                                    otp_verified: true,
                                    authtoken: token
                                }
                            })
                            .exec(function (err, cpu) {
                                if (err) {
                                    callback({
                                        "response_code": 5005,
                                        "response_message": "INTERNAL DB ERROR",
                                        "response_data": {}
                                    })
                                }
                                if (!err) {
                                    if (cpu.n === 1 && cpu.nModified === 1) {
                                        callback({
                                            "response_code": 2000,
                                            "response_message": "OTP verified successfully",
                                            "response_data": {
                                                "authtoken": token
                                            }
                                        })
                                    }
                                }
                            })

                    } else {
                        callback({
                            "response_code": 4001,
                            "response_message": "Invalid OTP!",
                            "response_data": {}
                        })
                    }
                } else {
                    callback({
                        "response_code": 5002,
                        "response_message": "OTP already verified/expired!",
                        "response_data": {}
                    })
                }
            }
        })
    }

    return s;

}());

module.exports = UserModels;