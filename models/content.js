var mongoose = require("mongoose");
var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var async = require("async");
var mongo = require('mongodb');
var ObjectID = mongo.ObjectID;
var mongoosePaginate = require('mongoose-paginate');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

// Export your module
var ContentModels = mongoose.model("Content", function () {
    var s = new mongoose.Schema({
        _id: {
            type: String,
            required: true
        },

        title: {
            type: String
        },
        page_french: {
            type: String,
            required: true
        },
        page_english: {
            type: String,
            required: true
        },
        content_french: {
            type: String,
            required: true
        },
        content_english: {
            type: String,
            required: true
        },
        status: {
            type: Boolean,
            enum: [
                true, false
            ],
            default: true

        },
        is_deleted: {
            type: Boolean,
            enum: [
                true, false
            ],
            default: false
        }


    }, {
        timestamps: true
    });
    s.plugin(mongooseAggregatePaginate);
    s.plugin(mongoosePaginate);


    s.statics.createContent = function (content, callback) {
        content._id = new ObjectID
        new ContentModels(content).save(function (err, response_data) {
            if (!err) {
                console.log("[comment added]");
                callback(response_data);
            } else {
                console.log(err);
                callback(err)
            }
        })
    }
    s.statics.contents = function (callback) {
        ContentModels.find({
            is_deleted: false
        }, function (err, contents) {
            if (!err) {
                console.log("[comment added]");
                callback({
                    response_code: 200,
                    response_data: {
                        status: true,
                        message: "Contents List",
                        data: contents
                    }
                });
            } else {
                console.log(err);
                callback({
                    response_code: 500,
                    response_data: {
                        status: false,
                        message: "Internal DB eror",
                        data: {}
                    }
                });
            }
        })
    }
    s.statics.updateContent = function (content, callback) {
        ContentModels.findByIdAndUpdate(content.id, content, {
            new: 1
        }, function (err, replaced) {
            if (!err) {

                callback(replaced);

            } else {
                callback(err)
            }
        })
    }
    s.statics.fetchContent = function (content, callback) {
        ContentModels.findById(content.contentId, function (err, replaced) {
            if (!err) {

                callback(replaced);

            } else {
                callback(err)
            }
        })
    }
    s.statics.deleteContent = function (content, callback) {
        content.is_deleted = false;
        ContentModels.findByIdAndUpdate(content.contentId, content, function (err, replaced) {
            if (!err) {

                callback(replaced);

            } else {
                callback(err)
            }
        })
    }

    return s;

}());

module.exports = ContentModels;